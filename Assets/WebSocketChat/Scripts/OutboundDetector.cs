﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutboundDetector : MonoBehaviour
{
    [SerializeField] private GameController gameController;

    public void SetGameeController(GameController gc)
    {
        gameController = gc;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag != "Message") return;
        gameController.ReleaseMessage(other.gameObject);
    }
}

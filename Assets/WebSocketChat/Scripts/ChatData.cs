﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ChatEntity
{
#pragma warning disable IDE1006 // 命名スタイル
    public string name;
    public string content;
    public string id;
    public string sender;
    public string createdAt;
    public string sessionId;
#pragma warning restore IDE1006 // 命名スタイル
}

[Serializable]
public enum ChatCommand
{
    APPEND, CLEAR, DELETE, SET, INFO, ERROR
}

[Serializable]
public class ChatContext : ISerializationCallbackReceiver
{
#pragma warning disable IDE1006 // 命名スタイル
    [SerializeField] private string command;
    public ChatEntity entity;
    [SerializeField] private ChatEntity[] messages;
    private List<ChatEntity> messagesList;
#pragma warning restore IDE1006 // 命名スタイル

    public ChatCommand Command
    {
        get { return (ChatCommand)Enum.Parse(typeof(ChatCommand), command); }
        set { command = value.ToString(); }
    }

    public List<ChatEntity> Messages
    {
        get { return messagesList; }
        set { messagesList = value; }
    }

    public void OnBeforeSerialize()
    {
        if (messagesList == null || messagesList.Count == 0) return;

        messages = messagesList.ToArray();
    }

    public void OnAfterDeserialize()
    {
        if (messages == null || messages.Length == 0) return;

        messagesList = new List<ChatEntity>(messages);
    }
}


[Serializable]
public class ContextWrapper
{
#pragma warning disable IDE1006 // 命名スタイル
    public ChatContext[] contexts;
#pragma warning restore IDE1006 // 命名スタイル
}

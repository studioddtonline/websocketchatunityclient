﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;

public class StompFrame
{
    public string[] Headers { get; private set; } //TODO Disctionaryにすべき
    public string Body { get; private set; }
    public StompCommand Command { get; private set; }

    private const char NullStr = (char)0x0;
    private const char LfStr = (char)0xa;

    // デフォルトコンストラクタ
    // CONNECTを返す
    public StompFrame()
    {
        Command = StompCommand.CONNECT;
        Headers = new string[] { "accept-version:1.1,1.0", "heart-beat:0,0" };
        Body = "";
    }

    // 受信用コンストラクタ
    public StompFrame(byte[] bytes)
    {
        ParseStompFrame(bytes);
    }

    // SUBSCRIBE用コンストラクタ
    public StompFrame(int index, string channel)
    {
        Command = StompCommand.SUBSCRIBE;
        Headers = new string[] { "id:sub-" + index, "destination:" + channel };
        Body = "";
    }

    // SEND用コンストラクタ
    public StompFrame(string[] headers, string body)
    {
        Command = StompCommand.SEND;
        Headers = headers;
        Body = body;
    }

    public override string ToString()
    {
        // content-length生成
        int contentLength = 0;
        if (!string.IsNullOrEmpty(Body))
        {
            byte[] octetData = System.Text.Encoding.UTF8.GetBytes(Body);
            contentLength = octetData.Length;
        }
        else Body = "";
        // header部生成
        var header = string.Join(LfStr, Headers) + LfStr;
        if (contentLength > 0)
        {
            header += "content-length:" + contentLength + LfStr;
        }
        // 戻す
        return Command.ToString() + LfStr + header + LfStr + Body + NullStr;
    }

    private void ParseStompFrame(byte[] bytes)
    {
        // STOMP仕様では終端がNULL文字（bytes.Length - 1）
        // ボディなしの場合はLFで分割（byte.Length - 2）
        // その場合のヘッダ終端がLF（byte.Length - 3）となるので
        // bytes.Length - 2までを探す
        var headerDelimiterIndex = 0;
        for (var i = 0; i < bytes.Length - 2; i++)
        {
            if (bytes[i] == 0xa && bytes[i + 1] == 0xa)
            {
                headerDelimiterIndex = i;
                break;
            }
        }
        // 検索した分割位置の前後でbyte配列をコピーする
        var headerOctets = bytes[..headerDelimiterIndex];
        var bodyOctets = bytes[(headerDelimiterIndex + 2)..(bytes.Length - 1)];
        // ヘッダ部はLFで分割してCommandとヘッダに分ける
        var splitedHeader = System.Text.Encoding.UTF8.GetString(headerOctets).Split(LfStr);
        Command = (StompCommand)Enum.Parse(typeof(StompCommand), splitedHeader[0]);
        Headers = splitedHeader[1..];
        Body = System.Text.Encoding.UTF8.GetString(bodyOctets);
    }
}

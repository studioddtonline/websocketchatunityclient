﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections;
using UnityEngine;
using TMPro;

public class MessageBoard : MonoBehaviour
{
    public TMP_Text NameText;
    public TMP_Text ContentText;
    [SerializeField] private Transform ContentMesh;

    private string _name;
    public string Name
    {
        get { return _name; }
        private set
        {
            NameText.text = _name = value;
        }
    }

    private string _content;
    public string Content
    {
        get { return _content; }
        private set
        {
            ContentText.text = _content = value;
        }
    }
    public string Id { get; private set; }
    public string Sender { get; private set; }
    public string CreatedAt { get; private set; }
    public bool IsOwner { get; private set; }

    public void SetChatEntity(ChatEntity entity, bool isOwner)
    {
        Name = entity.name;
        Content = entity.content;
        Id = entity.id;
        Sender = entity.sender;
        CreatedAt = entity.createdAt;
        IsOwner = isOwner;

        StartCoroutine(UpdateScale());
    }

    private IEnumerator UpdateScale()
    {
        // 1フレーム待つ
        while (ContentText.bounds.size.y == 0)
        {
            yield return null;
        }
        // 1フレーム待たないとTMP_Text.textに投入した文字列がバウンディングボックスやRenderedValuesに反映されない
        Debug.Log($"MB: RenderedValues: {ContentText.GetRenderedValues()}");
        // TODO TMP_Text.GetRenderedValues()あたりでレンダリング後のテキストサイズがわかるので、これに合わせてメッシュの大きさを変更すべき
        // TODO Ownerかどうかを渡されているので、Material.colorの切り替えでいけると思う
    }

    private void OnEnable()
    {
        NameText.text = "";
        ContentText.text = "";
    }

    private void OnDisable()
    {
        NameText.text = "";
        ContentText.text = "";
    }
}

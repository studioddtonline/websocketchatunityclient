﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using NativeWebSocket;

public class StompClient : MonoBehaviour, IDisposable
{
    public static string Name = "STOMP_CLIENT";
#if UNITY_EDITOR
    private const string WebSocketUrl = "ws://localhost:8080/stompOverWebsocket";
#else
    private const string WebSocketUrl = "ws://175.105.50.191:33910/stompOverWebsocket";
#endif
    private WebSocket _ws;
    public bool Established { get; private set; } = false;
    public string SessionId { get; private set; } = "";
    public string SenderName { get; private set; } = "";

    public delegate void ChatEventHandler(ChatEntity message);
    public delegate void ClientEventhandler(bool success);
    public event ChatEventHandler OnRecieveAppend;
    public event ChatEventHandler OnRecieveDelete;
    public event ClientEventhandler OnReady;
    public event ClientEventhandler OnClose;

    private async void Start()
    {
        // WebSocket生成
        // NativeWebSocketはUpdateでDispatchMessageQueueをコールしないとOnMessageが発火しないため
        // Updateより前にインスタンスが存在している必要がある
        _ws = new WebSocket(WebSocketUrl, new List<string>() { "v10.stomp", "v11.stomp" });
        // Stomp接続とチャット初期化
        if (await Connect() && await ChatInit())
        {
            Debug.Log($"STOMP: Established");
            _ws.OnMessage += OnWebsocketMessage;
            OnReady(true);
        }
        else
        {
            Debug.Log($"STOMP: Fail to connect");
            OnReady(false);
            OnClose(true);
        }
    }

    private void Update()
    {
        // NativeWebSocketはUpdateでDispatchMessageQueueをコールしないとOnMessageが発火しないため
        // Updateでたたき続けないとならない
        _ws.DispatchMessageQueue();
    }

    private async void OnDestroy()
    {
        // 先にOnCloseイベントを発火しておく
        try
        {
            OnClose(true);
        }
#pragma warning disable CS0168 // 変数は宣言されていますが、使用されていません
        catch (NullReferenceException e)
#pragma warning restore CS0168 // 変数は宣言されていますが、使用されていません
        {
            // no behavior
        }
        // イベントハンドラ登録削除、NativeWebSocket.Closeなど
        Established = false;
        _ws.OnClose -= OnWebsocketClose;
        _ws.OnError -= OnWebsocketError;
        _ws.OnMessage -= OnWebsocketMessage;
        await _ws.Close();
        _ws = null;
    }

    private Task<bool> Connect()
    {
        // 非同期で結果を返すためのTaskCompletionSource
        TaskCompletionSource<bool> source = new();
        // NativeWebSoket用のイベントハンドラ登録
        _ws.OnClose += OnWebsocketClose;
        _ws.OnError += OnWebsocketError;
        // OnOpen
        void OnOpen()
        {
            // OnOpen解除
            _ws.OnOpen -= OnOpen;
            // OnMessage
            void OnStompConnect(byte[] bytes)
            {
                // OnStompConnect解除
                _ws.OnMessage -= OnStompConnect;
                // 受信したbyte[]をパース
                StompFrame result = new(bytes);
                // STOMP接続成立の確認
                if (result.Command == StompCommand.CONNECTED)
                {
                    Established = true;
                }
                // Task完了
                source.SetResult(Established);
            }
            // OnStompConnect登録
            _ws.OnMessage += OnStompConnect;
            // Stomp ConnectをSEND
            _ws.SendText(new StompFrame().ToString());
        }
        // OnOpen登録
        _ws.OnOpen += OnOpen;
        // Connect実施
        _ws.Connect();
        // Task戻す
        return source.Task;
    }

    private Task<bool> ChatInit()
    {
        // 非同期で結果を返すためのTaskCompletionSource
        TaskCompletionSource<bool> source = new();
        // SUBSCRIBEはACKを求めないので一気に送信
        var index = 0;
        foreach (var channel in new string[] { "/topic/message", "/topic/clean", "/user/queue/init" })
        {
#pragma warning disable CS4014 // この呼び出しは待機されなかったため、現在のメソッドの実行は呼び出しの完了を待たずに続行されます
            Subscribe(index++, channel);
#pragma warning restore CS4014 // この呼び出しは待機されなかったため、現在のメソッドの実行は呼び出しの完了を待たずに続行されます
        }
        // OnStompInit
        void OnStompInit(byte[] bytes)
        {
            // OnStompInit解除
            _ws.OnMessage -= OnStompInit;
            // 受信したbyte[]をパース
            StompFrame result = new(bytes);
            // デシリアライズする
            ContextWrapper data = JsonUtility.FromJson<ContextWrapper>(result.Body);
            // 結果を格納
            var success = false;
            foreach (ChatContext context in data.contexts)
            {
                if (context.Command == ChatCommand.SET)
                {
                    SessionId = context.entity.sessionId;
                    SenderName = context.entity.name;
                    success = true;
                }
                else if (context.Command == ChatCommand.APPEND) AppendRunner(context);
            }
            source.SetResult(success);
        }
        // OnStompInit登録
        _ws.OnMessage += OnStompInit;
        // SEND Init
        _ws.SendText(new StompFrame(new string[] { "destination:/app/init" }, "{}").ToString());
        // Task戻す
        return source.Task;
    }

    public async Task Subscribe(int index, string channel)
    {
        Debug.Log("STOMP: Subscribe: " + channel);
        await _ws.SendText(new StompFrame(index, channel).ToString());
    }

    public async Task Send(ChatEntity message)
    {
        message.sender = SessionId;
        var textMessage = JsonUtility.ToJson(message);
        Debug.Log("STOMP: Send: " + textMessage);
        await _ws.SendText(new StompFrame(new string[] { "destination:/app/message" }, textMessage).ToString());
    }

    private void OnWebsocketClose(WebSocketCloseCode code)
    {
        Debug.Log($"STOMP: OnClose: {code.ToString()}");
        Established = false;
    }

    private void OnWebsocketError(string errorMsg)
    {
        Debug.Log($"STOMP: OnError: {errorMsg}");
    }

    // NativeWebSocket.OnMessageへのコールバック
    private void OnWebsocketMessage(byte[] bytes)
    {
        // 受信したbyte[]をパース
        StompFrame result = new(bytes);
        // デシリアライズする
        ContextWrapper data = JsonUtility.FromJson<ContextWrapper>(result.Body);
        // ランナーに流す
        CommandRunner(data.contexts);
    }

    private void CommandRunner(ChatContext[] contexts)
    {
        foreach (ChatContext context in contexts)
        {
            switch (context.Command)
            {
                case ChatCommand.APPEND:
                    AppendRunner(context);
                    break;
                case ChatCommand.DELETE:
                    DeleteRunner(context);
                    break;
                case ChatCommand.CLEAR:
                case ChatCommand.INFO:
                case ChatCommand.ERROR:
                default:
                    break;
            }
        }
    }

    private void AppendRunner(ChatContext context)
    {
        foreach (ChatEntity message in context.Messages)
        {
            OnRecieveAppend(message);
        }
    }

    private void DeleteRunner(ChatContext context)
    {
        foreach (ChatEntity message in context.Messages)
        {
            OnRecieveDelete(message);
        }
    }

    public void Dispose()
    {
        // OnDestoryに削除を任せる
        Destroy(this);
    }
}

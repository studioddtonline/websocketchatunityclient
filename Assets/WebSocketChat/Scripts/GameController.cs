﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using UnityEngine;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using UnityEngine.UI;
using UnityEngine.Pool;
using TMPro;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameController : MonoBehaviour
{
    private StompClient _sc;
    [SerializeField] private Camera mainCamera;
    [SerializeField] private GameObject ownerMessagePrefab;
    [SerializeField] private GameObject otherMessagePrefab;

    // TODO イベントシステム経由で受け取るようにする
    [SerializeField] private Button sendButton;
    [SerializeField] private TMP_InputField nameInputField;
    [SerializeField] private TMP_InputField contentInputField;

    private readonly ConcurrentQueue<ChatEntity> _messageQueue = new();
    private readonly ConcurrentQueue<GameObject> _deleteQueue = new();
    private readonly ConcurrentDictionary<string, GameObject> _visibleDictionary = new();

    private ObjectPool<GameObject> _ownerMessagePool;
    private ObjectPool<GameObject> _otherMessagePool;

    // ドラッグ検知で使う
    // TODO キモイのでEventSystems経由でやりたい
    private readonly RaycastHit[] hits = new RaycastHit[16];
    private Vector3 startPos = Vector3.zero;
    private Vector3 amountSwipe = Vector3.zero;
    private Rigidbody target = null;
    [SerializeField] GameObject outboundDetector;

    /// <summary>
    /// アプリを終了させるstaticメソッド
    /// </summary>
    public static void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_STANDALONE
        UnityEngine.Application.Quit();
#endif
    }

    /// <summary>
    /// シーン全体の設定と必須設定の確認をする
    /// </summary>
    private void Awake()
    {
        // シーンのフレームレートを30fpsに設定する
        Application.targetFrameRate = 30;
        // Inspectorからカメラが設定されていなければ終了させる
        if (mainCamera == null)
        {
            Quit();
        }
        // メッセージプレハブがアタッチされていなければ終了
        // TODO Addressableからの動的ロードに変更する
        if (ownerMessagePrefab == null || otherMessagePrefab == null)
        {
            Quit();
        }
    }

    /// <summary>
    /// StompClient作成とアセット読み込みを行う
    /// </summary>
    private async void Start()
    {
        // StompClient初期化
        if (!(await InitStompClient()))
        {
            Quit();
        }
        // TODO Addressableからリソース読み込みする

        // メッセージ用のオブジェクトプールを作る
        _ownerMessagePool = PoolFactory(ownerMessagePrefab);
        _otherMessagePool = PoolFactory(otherMessagePrefab);

        // 雑にボタンにリスナーをつけて送信
        // TODO 雑すぎるのでイベント経由にすべき
        sendButton.onClick.AddListener(async () =>
        {
            ChatEntity message = new();
            message.name = nameInputField.text;
            message.content = contentInputField.text;
            await _sc.Send(message);
        });

        // アウトバウンド検出用のコライダーを生成する
        if (outboundDetector == null)
        {
            Quit();
        }
        else
        {
            // TODO プレハブ呼び出しでダサいのでAddressableで動的ロードにしたい
            var od = Instantiate(outboundDetector, transform);
            od.GetComponent<OutboundDetector>().SetGameeController(this);
        }
    }

    /// <summary>
    /// メッセージの表示と削除、およびユーザインタラクションの受付を行う
    /// </summary>
    private void Update()
    {
        // メッセージキューが空でなければメッセージインスタンスを生成する
        // TODO オブジェクトプールを使う
        if (!_messageQueue.IsEmpty && _messageQueue.TryDequeue(out ChatEntity messageSource))
        {
            // ChatEntity.senderがセッションIDと同じなら自分のメッセージ
            // TODO 似たようなオブジェクトが複数あるのは非効率なのでMaterial.colorの切り替え等で対応できるようにすべき
            bool isOwner = messageSource.sender.Equals(_sc.SessionId);
            ObjectPool<GameObject> pool;
            if (isOwner)
            {
                pool = _ownerMessagePool;
            }
            else
            {
                pool = _otherMessagePool;
            }
            // メッセージインスタンス取得
            var message = pool.Get();
            message.transform.position = new Vector3(0, 71, Random.Range(-80.0f, 80.0f));
            message.transform.rotation = Quaternion.Euler(Random.Range(-5.0f, 5.0f), Random.Range(-5.0f, 5.0f), Random.Range(-5.0f, 5.0f));
            // 表示中ディクショナリにキャッシュ
            if (!_visibleDictionary.TryAdd(messageSource.id, message))
            {
                Debug.Log($"GC: fail to add _visibleDictionary");
            }
            // MessageBoardにmessageの内容を書き込む
            message.GetComponent<MessageBoard>().SetChatEntity(messageSource, isOwner);
        }
        // 削除キューが空でなければメッセージインスタンスを削除する
        if (!_deleteQueue.IsEmpty && _deleteQueue.TryDequeue(out GameObject deleteTarget))
        {
            ReleaseToPool(deleteTarget);
        }
    }

    private void FixedUpdate()
    {
        // TODO ドラッグ検知
        DetectSwipe();
    }

    /// <summary>
    /// アプリとしてのデストラクタ
    /// </summary>
    private void OnDestroy()
    {
        _sc.OnRecieveAppend -= OnRecieveAppend;
        _sc.OnRecieveDelete -= OnRecieveDelete;
        Destroy(_sc);
    }

    /// <summary>
    /// StompClientを作成してイベントハンドラを登録する
    /// </summary>
    /// <returns></returns>
    private Task<bool> InitStompClient()
    {
        // TaskCompletionSource生成
        TaskCompletionSource<bool> source = new();
        // 既存がないか確認
        if (_sc != null)
        {
            source.TrySetCanceled();
            return source.Task;
        }
        // StompSession生成
        _sc = new GameObject(StompClient.Name).AddComponent<StompClient>();
        // ヒエラルキー上で邪魔なので親を設定
        _sc.transform.SetParent(transform);
        // イベントハンドラ登録
        void OnReady(bool success)
        {
            _sc.OnReady -= OnReady;
            // UIに反映
            nameInputField.text = _sc.SenderName;
            // 結果格納
            source.SetResult(success);
        }
        _sc.OnReady += OnReady;
        _sc.OnRecieveAppend += OnRecieveAppend;
        _sc.OnRecieveDelete += OnRecieveDelete;
        // Task戻す
        return source.Task;
    }

    /// <summary>
    /// StompClientのイベントハンドラ
    /// Appendメッセージを受け取ってキューに詰める
    /// </summary>
    /// <param name="message">イベント経由で得られるChatEntity</param>
    private void OnRecieveAppend(ChatEntity message)
    {
        _messageQueue.Enqueue(message);
    }

    /// <summary>
    /// StompClientのイベントハンドラ
    /// Deleteメッセージを受け取って、対象のメッセージインスタンスを表示ディクショナリから得て、削除キューに詰める
    /// </summary>
    /// <param name="message">メッセージのChatEntity</param>
    private void OnRecieveDelete(ChatEntity message)
    {
        if (_visibleDictionary.TryRemove(message.id, out GameObject result))
        {
            _deleteQueue.Enqueue(result);
        }
    }

    /// <summary>
    /// オブジェクトプールを作るファクトリ
    /// </summary>
    /// <param name="prefab">プレハブ</param>
    /// <returns>プレハブのオブジェクトプール</returns>
    private ObjectPool<GameObject> PoolFactory(GameObject prefab)
    {
        return new ObjectPool<GameObject>(
            () => { return Instantiate(prefab, transform); },
            (o) =>
            {
                o.SetActive(true);
            },
            (o) =>
            {
                o.SetActive(false);
                Rigidbody rigidbody = o.GetComponent<Rigidbody>();
                rigidbody.ResetInertiaTensor();
                rigidbody.velocity = Vector3.zero;
                rigidbody.rotation = Quaternion.identity;
            },
            (o) => { Destroy(o); },
            false,
            10,
            100
        );
    }

    public async void ConnectStompClient()
    {
        await InitStompClient();
    }

    public void DisconnectStompClient()
    {
        _sc?.Dispose();
        Destroy(_sc);
        _sc = null;
    }

    /// <summary>
    /// やっつけでつくったのでイベント通知などに変更すべき
    /// 外部に飛んでいくメッセージをプールに戻す
    /// </summary>
    public void ReleaseMessage(GameObject outboundGameObject)
    {
        ReleaseToPool(outboundGameObject);
    }

    private void ReleaseToPool(GameObject releaseTarget)
    {
        // オーナーかどうかを取得
        bool isOwner = releaseTarget.GetComponent<MessageBoard>().IsOwner;
        // プールの指定
        ObjectPool<GameObject> pool;
        if (isOwner)
        {
            pool = _ownerMessagePool;
        }
        else
        {
            pool = _otherMessagePool;
        }
        // オブジェクトのリリース
        pool.Release(releaseTarget);
    }

    private void DetectSwipe()
    {
        // TODO もっとどうにかしたい
        // タッチパネルのタッチカウントを確認する
        if (Input.touchCount > 0)
        {
            // カレントのタッチを取得
            Touch currentTouch = Input.GetTouch(0);
            // TouchPhase.Beginなら位置を保存
            if (currentTouch.phase == TouchPhase.Began)
            {
                // タッチのポジションを取得
                Vector3 currentTouchPosition = currentTouch.position;
                // メインカメラからのレイを作成
                Ray ray = Camera.main.ScreenPointToRay(currentTouchPosition);
                // レイとコライダーとのコリジョンを確認
                int hitNum = Physics.RaycastNonAlloc(ray, hits);
                // コリジョンを確認した上でTouchPhaseを確認する
                if (hitNum > 0)
                {
                    startPos = currentTouch.position;
                    target = hits[0].rigidbody;
                }
            }
            // TouchPhase.Endedならスワイプ量を計算
            else if (currentTouch.phase == TouchPhase.Ended)
            {
                if (startPos != Vector3.zero)
                {
                    amountSwipe = (Vector3)currentTouch.position - startPos;
                    startPos = Vector3.zero;
                }
                else
                {
                    startPos = Vector3.zero;
                    amountSwipe = Vector3.zero;
                }
            }
        }
        // Editor上の場合
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                // タッチのポジションを取得
                Vector3 currentTouchPosition = Input.mousePosition;
                // メインカメラからのレイを作成
                Ray ray = mainCamera.ScreenPointToRay(currentTouchPosition);
                // レイとコライダーとのコリジョンを確認
                // レイヤーマスクでMessageレイヤにだけレイを当てる
                int hitNum = Physics.RaycastNonAlloc(ray, hits, Mathf.Infinity, 1 << 6);
                // コリジョンを確認した上でTouchPhaseを確認する
                if (hitNum > 0)
                {
                    startPos = Input.mousePosition;
                    target = hits[0].rigidbody;
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                amountSwipe = (Vector3)Input.mousePosition - startPos;
            }
        }
        // スワイプ量に合わせてtransformにパワーをあたえる
        if (amountSwipe != Vector3.zero && target != null)
        {
            // やけくそで10倍のパワーをあたえる
            target.AddForce(new Vector3(amountSwipe.x, 0, amountSwipe.y) * 10, ForceMode.Acceleration);
            target = null;
            amountSwipe = Vector3.zero;
            startPos = Vector3.zero;
        }
    }

    public void SendMessage()
    {

    }
}
